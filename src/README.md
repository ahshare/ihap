# Dependencies
***
### Dependencies in Administrator system:
* This packages MUST be installed in the administrator desktop system, not in the server. The packages will give you the possibility to execute remote commands by SSH using python (fabric and cuisine modules).

        apt-get install git
        apt-get install fabric
        apt-get install python-pip
        apt-get install python-setuptools
        apt-get install python-jinja2
        easy_install cuisine

### Dependencies in Server:
* The server MUST be Ubuntu 12.04 LTS or Debian 7.0 with the following packages installed.

        apt-get install openssh-server

* In Debian system, ensure that you have an user with sudo permissions:

        apt-get install sudo
        usermod -a -G sudo <user with ssh access and sudo permissions>

# Deployment
***
* In Administrator system, run the following commands to clone the repository. 

        cd /tmp/
        git clone https://bitbucket.org/ahshare/ihap.git

* Check the configuration file (setup.conf), with special attention in *General*, *EjabberdAccounts* and *Specifications* sections.

* To install, run the following command (change the values between '<>'):

        cd /tmp/ihap/src/
        fab -f setup.py install --user=<user with ssh access and sudo permissions> --password=<sudo user password> --hosts=<server>

* Example:

        fab -f setup.py install --user=default --password=xyz123 --hosts=server.local

# Run
***
* Run the following commands:

        sudo /etc/init.d/elasticsearch start

* Wait 30 seconds to make sure that elasticsearch has time to initiate

        sudo /etc/init.d/logstash start
		
* Wait 30 seconds to make sure that logstash has time to initiate

        sudo /etc/init.d/ejabberd start

* Wait 30 seconds to make sure that ejabberd has time to initiate

        sudo /etc/init.d/apache2 start
		sudo /etc/init.d/abusehelper start
        

# Visualization
***
* Access with your browser:

        http:// < your_server_ip_or_fqdn \>
        http:// < your_server_ip_or_fqdn \> /index.html#/dashboard/file/guided.json
        
* Download IHAP dashboard and load it into Kibana (http:// < your_server_ip_or_fqdn \>) - [download](https://bitbucket.org/ahshare/ihap/downloads/IHAP-dashboard)

* See the [Kibana tutorials](http://www.elasticsearch.org/guide/en/kibana/current/)


# Additional Information
***

* Check setup.conf to find all information related to software installation and configuration path's.
