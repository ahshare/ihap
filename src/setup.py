import time
import ConfigParser
from cuisine import *
from fabric.api import env
from modules.kibana import Kibana
from modules.logstash import LogStash
from modules.ejabberd import Ejabberd
from modules.elasticsearch import ElasticSearch
from modules.abusehelper import AbuseHelper

configurations = ConfigParser.ConfigParser()
configurations.read("./setup.conf")

def install():
    print
    print "\tWarning"
    print "\t-------"
    print "\t    * Ensure that you already installed the package 'openssh-server' in the server."    
    print "\t    * Ensure that you have the correct configurations in the server:"
    print "\t\t    - comapare the configuration file '/etc/hosts' in server with 'setup.confg'"
    print "\t\t    - comapare the configuration file '/etc/hostname' in server with 'setup.confg'"
    print
    raw_input("\t[Press Enter to continue]")
    print 

    __install()

    print
    print
    print "\tInitialization"
    print "\t--------------"
    print "\t    * Start the services '/etc/init.d/' in the following order but ensure that you give time for each service to startup:"
    print "\t\t    - elasticsearch"
    print "\t\t    - logstash"
    print "\t\t    - ejabberd"
    print "\t\t    - abusehelper"
    print "\t\t    - apache2"
    print

def __install():
    sudo("apt-get update")
    
    elasticsearch = ElasticSearch(configurations)
    logstash = LogStash(configurations)
    kibana = Kibana(configurations)
    ejabberd = Ejabberd(configurations)
    abusehelper = AbuseHelper(configurations)

    elasticsearch.install()
    logstash.install()
    kibana.install()    
    ejabberd.install()    
    abusehelper.install()
