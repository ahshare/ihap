from cuisine import *
from fabric.context_managers import settings
import jinja2
import time

class Ejabberd():

    def __init__(self, configurations):
        self.core_host = configurations.get('General','host')
        self.ejabberd_admin = configurations.get('EjabberdAccounts','administrator_user')
        self.ejabberd_admin_password = configurations.get('EjabberdAccounts','administrator_user_password')
        self.ejabberd_user = configurations.get('EjabberdAccounts','abusehelper_user')
        self.ejabberd_user_password = configurations.get('EjabberdAccounts','abusehelper_user_password')
        self.ejabberd_room_service = configurations.get('Specifications','abusehelper_ejabberd_room')
        self.ejabberd_configuration_file_template = configurations.get('Templates','ejabberd_configuration')
        self.ejabberd_configuration_file = configurations.get('Configurations','ejabberd')


    def install(self):
        with mode_sudo(True):
            self.__package_install()
            self.stop()
            self.__update_ejabberd_config()
            # FIXME: self.__create_certificate()
            self.start()
            self.__create_ejabberd_account()


    def update(self):
        pass


    def start(self):
        upstart_ensure('ejabberd')


    def stop(self):
        upstart_stop('ejabberd')
        with settings(warn_only=True):
            sudo("pkill -f erlang")        


    def __package_install(self):
        package_ensure('ejabberd')


    def __create_ejabberd_account(self):
        time.sleep(2)
        with settings(warn_only=True):
            registered_users = sudo("/usr/sbin/ejabberdctl registered_users %s" % self.core_host)
            if not self.ejabberd_user in registered_users:
                print "Creating a Jabber account."
                sudo("/usr/sbin/ejabberdctl register %s %s %s" % (self.ejabberd_user, self.core_host, self.ejabberd_user_password))


    def __update_ejabberd_config(self):
        self.stop()
        
        # Move original configuration
        if file_exists(self.ejabberd_configuration_file):
            sudo("mv %s %s.orig" % (self.ejabberd_configuration_file, self.ejabberd_configuration_file))
        
        # Write new configuration
        fptemplate = open(self.ejabberd_configuration_file_template, 'r')
        template_content = fptemplate.read()
        config_content = template_content.replace('{{ hostname }}', self.core_host)
        file_write(self.ejabberd_configuration_file, config_content)


    def __create_ejabberd_certificate(self):
        pass
