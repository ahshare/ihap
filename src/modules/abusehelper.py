from cuisine import *
from fabric.context_managers import settings
import jinja2
import time

class AbuseHelper():

    def __init__(self, configurations):
        self.core_packages_dir = configurations.get('Packages','directory')
        self.core_host = configurations.get('General','host')
        self.ejabberd_user = configurations.get('EjabberdAccounts','abusehelper_user')
        self.ejabberd_user_password = configurations.get('EjabberdAccounts','abusehelper_user_password')
        self.ejabberd_room_service = configurations.get('Specifications','abusehelper_ejabberd_room')
        self.ah_package_url = configurations.get('Packages','abusehelper')
        self.ah_user = configurations.get('Specifications','abusehelper_system_user')        
        self.ah_installation_dir = configurations.get('Installation','abusehelper_directory')
        self.ah_lib_dir = os.path.join(self.ah_installation_dir, "production")
        self.ah_init_file = configurations.get('InitScripts','abusehelper')
        self.ah_init_file_template = configurations.get('Templates','abusehelper_initscript')

    def install(self):
        with mode_sudo(True):
            self.__dependencies()
            self.__package_install()
            self.__directories_install()
            self.__plugin_install_geoip()
            self.__update_init_script()

    def update(self):
        pass

    def start(self):
        sudo("%s start" % self.ah_init_file)

    def stop(self):
        sudo("%s stop" % self.ah_init_file)

    def __dependencies(self):
        package_ensure('mercurial')
        package_ensure('python')

    def __package_install(self):
        dir_ensure(self.core_packages_dir, recursive=True)
        with cd(self.core_packages_dir):
            sudo("hg clone %s" % self.ah_package_url)
        with cd("%s/abusehelper" % self.core_packages_dir):
            sudo("python setup.py install")

    def __directories_install(self):
        user_create(self.ah_user, fullname=AbuseHelper, createhome=True)
        dir_ensure(self.ah_installation_dir, recursive=True, mode="750", owner="root", group=self.ah_user)
        with cd("%s/abusehelper" % self.core_packages_dir):
            xmpp_jid = "%s@%s" % (self.ejabberd_user, self.core_host)
            ejabberd_ignore_cert = True # FIXME: need SSL code [Aaron]
            sudo("python abusehelper/contrib/config-generator/confgen.py %s %s %s %s %s" % (self.ah_lib_dir, xmpp_jid, self.ejabberd_user_password, ejabberd_ignore_cert, self.ejabberd_room_service))  
        dir_attribs(self.ah_lib_dir, mode="750", owner="root", group=self.ah_user, recursive=True)
        state_dir = os.path.join(self.ah_lib_dir, "state") 
        log_dir = os.path.join(self.ah_lib_dir, "log") 
        archive_dir = os.path.join(self.ah_lib_dir, "archive")
        dir_ensure(state_dir, recursive=True, mode="750", owner=self.ah_user, group=self.ah_user)
        dir_ensure(log_dir, recursive=True, mode="750", owner=self.ah_user, group=self.ah_user)
        dir_ensure(archive_dir, recursive=True, mode="750", owner=self.ah_user, group=self.ah_user)

    def __update_init_script(self):
        context = {
            "botuser": self.ah_user,
            "botnets": self.ah_lib_dir,
        }
        fptemplate = open(self.ah_init_file_template, 'r')
        template_content = fptemplate.read()
        config_content   = jinja2.Environment().from_string(template_content).render(context)
        file_write(self.ah_init_file, config_content)
        file_ensure(self.ah_init_file, mode="755", owner="root", group="root")
        sudo("update-rc.d -f abusehelper defaults")
        sudo("update-rc.d -f abusehelper enable")

    # GeoIP
    def __plugin_install_geoip(self):
        with cd(self.core_packages_dir):
            sudo("wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz")
            sudo("gunzip -f GeoLiteCity.dat.gz")
            geoip_dir = "/usr/local/share/GeoIP/"
            dir_ensure(geoip_dir, recursive=True, mode="750", owner="root", group=self.ah_user)
            sudo("mv GeoLiteCity.dat %s" % geoip_dir)
            sudo("easy_install pygeoip")
