#!/bin/sh

# assumption: you have a clean default Debian wheezy installed

# there is a good howto on bitbucket.org which is the source of these commands now:
# (https://bitbucket.org/ahshare/abusehelper-share/src/6be78c1ce98da9e1133405e895ac9ac7b6bdcc6d/installation/HOWTO?at=master)

HOSTNAME=$(hostname -f)


#################################################
##  [0] General system setup
#################################################
echo
echo "Installing haveged (for more entropy)... "
apt-get install haveged


#################################################
##  [1] Ejabberd Installation
#################################################
echo
echo "Ejabberd"
echo "========"

##  Install
echo "Installing ejabberd ..."
apt-get install ejabberd
invoke-rc.d ejabberd stop

##  Generate Certificate
##  -- TODO: aaron needs to work on this
##  -- FIXME: need path
echo "Generating certificate ..."
# we need entropy here:
./create-ssl-certs.sh  

##  Backup
echo "Backuping original configurations (.orig) ..."
cd /etc/ejabberd/
cp ejabberd.cfg ejabberd.cfg.orig
cp ejabberd.pem ejabberd.pem.orig

##  Generate Configuration
##  -- FIXME: need path
echo "Generating configuration ..."
sed -i "s/%%%HOSTNAME%%%/$HOSTNAME/g" ejabberd.cnf.patch
patch /etc/ejabberd/ejabberd.cnf ejabberd.cnf.patch

##  Start Ejabberd
echo "Ejabberd starting ..."
invoke-rc.d ejabberd start

##  Create Ejabberd Accounts
echo "Creating ejabberd accounts ..."
echo -n "  Administrator (administrator@"$HOSTNAME") password: "
read password
ejabberdctl register administrator $HOSTNAME $password

echo -n "  AbuseHelper (abusehelper@"$HOSTNAME") password: "
read password
ejabberdctl register abusehelper $HOSTNAME $password


#################################################
##  [2] AbuseHelper Installation
#################################################
echo
echo "AbuseHelper"
echo "==========="

##  Install AbuseHelper dependencies
echo "Installing abusehelper dependencies ..."
echo "  * Git ..."
echo "  * Mercurial ..."
echo "  * OpenSSL ..."
echo "  * Python 2.7 ..."
echo "  * Python-dateutil ..."
echo "  * Python-pip ..."
echo "  * Pygeoip ..."
apt-get install git mercurial openssl python2.7 python-dateutil python-pip -y > /dev/null
easy_install pygeoip > /dev/null

##  Install MaxMind GeoIP
echo "  * MaxMind (GeoLiteCity)..."
cd /tmp/
wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
gunzip GeoLiteCity.dat.gz
chown root.staff GeoLiteCity.dat
mkdir -p /usr/local/share/GeoIP/
mv GeoLiteCity.dat /usr/local/share/GeoIP/
echo "      - database is located at '/usr/local/share/GeoIP/'"

##  AbuseHelper Installation 
echo "Clonning abusehelper repository ..."
cd /root/
hg clone https://ahshare@bitbucket.org/ahshare/abusehelper-patched
cd abusehelper-patched
echo "Installing abusehelper ..."
python ./setup.py install
mkdir -p /var/lib/abusehelper
chown root:abusehelper /var/lib/abusehelper
chmod 750 /var/lib/abusehelper
echo "  * Creating 'abusehelper' user on the system ..."
useradd -m abusehelper

echo "  * Configuring ..."
python abusehelper/contrib/confgen/confgen.py /var/lib/abusehelper/production
#  IDEA: Ignore this step. Use sed to overwrite the values.
#        Best option: give the chance to user to choose if wants automatically or mannually.
#
#  FIXME: Configurations Menu
#         XMPP username: abusehelper@ah-dev.cert.at
#         XMPP password: xyz123
#         XMPP lobby channel: abuse
#         Configure Mailer? yes/[no]: no

chown -R abusehelper /var/lib/abusehelper/production/
chmod 750 /var/lib/abusehelper/production/

mkdir -p /var/lib/abusehelper/production/state/
mkdir -p /var/lib/abusehelper/production/log
chown abusehelper /var/lib/abusehelper/production/state/
chown abusehelper /var/lib/abusehelper/production/log/


#  FIXME:
#  change the startup.py file '/var/lib/abusehelper/production/startup.py'
#  1) uncomment the line '# xmpp_rate_limit=3'
#  2) uncomment the line 'bot_state_file=os.path.join("state", name + ".state")'
#  3) uncomment the line 'log_file=os.path.join("log", name + ".log")'
#  4) add a comma in the end of the line 'service_room=service_room'



##  Start AbuseHelper
##  =================

# FIXME: this command is not correct
sudo su - abusehelper -s /bin/bash

botnet start /var/lib/abusehelper/production/
botnet follow /var/lib/abusehelper/production/

