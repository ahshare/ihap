#!/bin/sh

HOSTNAME=$(hostname)
FQDN=$(hostname -f)
KEYSIZE=3072
# key size is based on the recommendations of ECRYPT2 2012: http://www.ecrypt.eu.org/


# TODO: 
# choice between self signed certificates or official ones
# choice to use existing stored keys or generate new ones
# add all intermediate CA certificates to the ejabberd.pem file !!
# Describe how people can check with xmpp.net


# generate self-signed certificates
#   openssl req -x509 -config ssl/openssl.cnf -newkey rsa:$KEYSIZE -keyout ssl/keys/$HOSTNAME.key -out ssl/certs/$HOSTNAME.pem  -nodes -days 3650
#  warn if you would overwrite it
#   cat ssl/keys/$HOSTNAME.key ssl/certs/$HOSTNAME.pem >> /etc/ejabberd/ejabberd.pem



# generate official certificate

cat << EOT
=======================================================
Generating Certificate key...

We are now going to generate a key and a SSL certificate
request. The resulting files will be placed in the ssl/
subdirectory

EOT


if [ -f ssl/keys/$HOSTNAME.key -o -f ssl/$HOSTNAME.csr ]; then
  echo "WARNING! you seem to already have a $HOSTNAME.csr or key. Do you wnat to continue? [yN]"
  read yn
  if [ "x$yn" != "xy" ]; then
   exit 1
  fi
fi

openssl genrsa -out ssl/keys/$HOSTNAME.key  $KEYSIZE
sed "s/%%%HOSTNAME%%%/$FQDN/g" < ssl/openssl.cnf.template > ssl/openssl.cnf
openssl req -new -config ssl/openssl.cnf -key ssl/keys/$HOSTNAME.key -out ssl/$HOSTNAME.csr && echo "  DONE"

cat <<EOT


Generated the certificate signing request file : ssl/$HOSTNAME.csr
Please copy & paste this now to your CA and let it sign this key.


EOT

cat ssl/$HOSTNAME.csr

cat << EOT3



Next step: you now need to get this generated certificate signing request (CSR)
signed by some valid Certificate Authority (CA). One example of how to do this
is by following the instructions for startssl.com:
https://github.com/BetterCrypto/duraconf/blob/master/startssl/README.markdown


Once you got the CSR signed by a CA, place it into directory ssl/certs and name
it $HOSTNAME.pem


!!!IMPORTANT!!!  
In addition please get *all* the intermediate CA certificates
from the CA, concatenate them and place them in the directory ssl/ under the
name "CA.pem".  This is required by ejabberd.

Example: XXX FIXME XXX add example
  

EOT3


echo -n "READY [y/n] ? "
read ready 

while [ ! -f $HOSTNAME.pem ]; do
  case $ready  in
	y|Y)
		break
		;;
	*)
		echo -n "READY? [yn] "
		read ready
		;;
  esac
done

# the keys must be concatenated
if [ -f /etc/ejabberd/ejabberd.pem ]; then
  cat <<EOT

!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!!!
You already have an SSL certificate for ejabberd 
We are going to move the old one out of the way
and rename it to /etc/ejabberd/ejabberd.pem.old
and then install our certificate on top

EOT
  cp /etc/ejabberd/ejabberd.pem /etc/ejabberd/ejabberd.pem.old
fi

cp ssl/keys/$HOSTNAME.key /etc/ejabberd/ejabberd.pem
cat ssl/certs/$HOSTNAME.pem >> /etc/ejabberd/ejabberd.pem
cat ssl/CA.pem >> /etc/ejabberd/ejabberd.pem

chown ejabberd.ejabberd /etc/ejabberd/ejabberd.pem


