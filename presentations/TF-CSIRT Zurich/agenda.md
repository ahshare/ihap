% Abushelper workshop: how to install, run and do data analysis with Abusehelper and ElasticSearch
 -- _a workshop for CERTs by CERTs_
% L. Aaron Kaplan <kaplan@cert.at> ; Tomás Lima <tomas.lima@cert.pt>
% 2014/02/13
---------------------------



# Part 1: Intro
Time: 9:00 --  10:00
Speakers: Aaron Kaplan, Tomás Lima, Cosmin Ciobanu



---
# Overview workshop and history

  1. Intro
  2. Background, History, Updates on our progress
  3. Demo time: playing around with Kibana, ElasticSearch
  4. _lunch break_
  5. Discussion: project plan & requirements 
  6. Getting Abusehelper + Kibana + ES to run on your own server
  7. wrap up
    
# Prerequisites

Participants should have a virtual machine ready at their own CERT.

  * Ubuntu 12.04 LTS Server or Debian Wheezy
  * Memory: 2GB or preferably more
  * CPU: 1 core or more
  * Disk: 20GB or preferably more
  * unfiltered Internet access for the VM
  * SSH access to the VM

# History
  * History of some "Abusehelper projects" in CERTs (in more or less historical order)
    - CERT.fi
    - CERT.ee
    - CERT.be
    - CERT.is
    - CERT-eu
    - CERT.at
    - CERT.pt
    - CERT.hk
    - ....
	- you?


# Re-cap Abusehelper: where we are standing now, what are the issues?

  * healthy interest in Abusehelper as a framework since a couple of years
  * multiple presentations (CERT.be, CERT.pt, ENISA, ...)
  * but! so far it is hard to deploy and maintain for many CERTs
  * Clarified sells consulting if needed
  * => idea: community edition?
  * some CERTs successfully use Abusehelper. Why? How?
  * initial attempts (CERT-eu) to cross connect with other CERTs via XMPP


# The Abusehelper Framework
  * General remarks
  * Strengths:
    * beautiful architecture
    * flow-oriented architecture
    * lots of existing bots to fetch data
    * loosely de-coupled: in **theory** easy to write new "bots" and extend Abusehelper
    * open source
  * Issues/Weaknesses:
    * code complexity. Are you a python guru?
    * Getting code upstream to Clarified
    * everything is parallel => hard to understand
    * resource-hog => how to improve on this?
    * no standard way to include into RTIR/OTRS

# The Abusehelper Framework
  ![roomgraph example](img/roomgraph.png)

# Alternatives to Abusehelper? 

**Watch these:**

  * Megatron: open source, Java. Aware of two CERTs using it [https://github.com/cert-se/megatron-java](https://github.com/cert-se/megatron-java)
  * n6: CERT.pl [http://n6.cert.pl/](http://n6.cert.pl/)
  * CIF: USA [http://csirtgadgets.org/](http://csirtgadgets.org/) 

# Successes and failures
  * Overview of who is successful and what failed
  Which steps were needed to get there?

  * CERT.fi, CERT.ee seem to be doing fine. Clarified is close by
  * CERT.eu seems to be doing fine. Clarified gives support
  * CERT.hk has Chris ;-)

# Overview of the general incident handling automation topic

  Current situation:

  * no standards
  * no clear documents available
  * ongoing work (together with CERT.org/Jeffrey C.) on abstract document 
  * Enisa's perspective?
  * Fact: CERTs use some kind of CSV to exchange incident reports
  * Fact: almost all CERTs use some kind of ticket system

# So what is needed?
  * standardization amongst CERTs (no, no XML, no iodef, KISS)
  * standardization _within_ Abusehelper
  * good documentation
  * a lively open source community
  * interfaces to other incident automation frameworks
  * "setup.exe". Make it easy for new CERTs to start
  * a clear way to integrate Abusehelper with ticket systems
  * contact lookups (TI DB? FIRST DB? contactDB)
  * _... your ideas? ..._

#AbuseHelper & ENISA
  * work so far
  * on-going projects & activities
  * perspectives

#When did it started?
  * in 2008 we started with the basic training scenarios for CERTs
  * in 2012 we’ve added some more scenarios (including AH setup)
  * In 2013 we gave some trainings on installing & running AH instance
 

[http://www.enisa.europa.eu/activities/cert/support/exercise](http://www.enisa.europa.eu/activities/cert/support/exercise)

#Current support
  * ENISA received two formal request to support AH initiatives

from  CERT.PT & CERT.AT 

  * Other National CERTs can join by submiting a req. 

#The good , the bad and the ugly
  * we've invited many people to join the initiative
  * started defining objectives and needs (what is missing, nice to have)
  * Dedicated page promoting the AH initiative (Data Harmonisation, automation of incident handling) 

#Recent developments
  * conf. call with Clarified Networks
  * current issues:
    * code submited not according to the coding framework
    * due diligence & copyright issues for customers (AH core part of commercial solution)
    * push requests from public repo are not accepted (easily)
  * potential solutions:
    * branch of the main repo up to date
  

# 

  * /ENISA 

# Potential solution - IHAP (Incident Handling Automation Project)
  * current project title 
  * combination of Tomas' and Aaron's setups
  * source: [https://bitbucket.org/ahshare/ihap/](https://bitbucket.org/ahshare/ihap/)

  Directory structure:

	IFAS/
		-docs/
		-presentations/
		-src/ifas
			-ifas-deployment-tool
			-logcollector


# Deployment tool: "Setup.exe" 
  * Contains: 
    * Abusehelper 
    * ejabberd 
    * ElasticSearch 0.90 
    * logstash 1.2.2
    * Kibana 3 
    * (future IFAS)
  * written in fabric/cuisine (python)

# Halp! Confused? Too many words?

![confused?](img/confused.png)

# It's not so complicated

  * Abusehelper  = the main Abusehelper setup by clarified. original source with minor modifications.
  Our patches go upstream.
  * ejabberd = chat server
  * ElasticSearch 0.90  = noSQL Database for logs
  * logstash 1.2.2 = log collector which sends to ES
  * Kibana 3  = web interface for ES
  * (future IFAS) = dashboard by CERT.hk
  * fabric/cuisine (python) = python tool for deployment

# Description of the components: Abusehelper
  * "AbuseHelper is an open-source project initiated by CERT.FI (Finland) and CERT.EE (Estonia) with ClarifiedNetworks to automatically process incidents notifications." [[wikipedia]](https://en.wikipedia.org/wiki/AbuseHelper)
  * source: [https://bitbucket.org/clarifiednetworks/abusehelper](https://bitbucket.org/clarifiednetworks/abusehelper)
  * written in python 2.x. Uses XMPP for "chatrooms" and a chat server for message passing between "bots"
  * "bots" (= programs) communicate with each other and filter messages, parse them and process them
  * highly configurable
  * needs adaptation for every CERT (=> consulting by Clarified)

# Description of the components: ejabberd
  * "ejabberd is an XMPP application server, written mainly in the Erlang programming language."  [[wikipedia]](https://en.wikipedia.org/wiki/Ejabberd)
  * Erlang = functional language. Highly parallel. Scales
  * the most popular XMPP server
  * supports clustering, live upgrades, ldap authentication, TLS/SSL 
  * has many modules
  * can be a bit tricky with SSL
  * supports server2server connections

# Description of the components: ElasticSearch

  * "Elasticsearch is a search server based on Lucene. It provides a distributed, multitenant-capable full-text search engine with a RESTful web interface and schema-free JSON documents. Elasticsearch is developed in Java" [[wikipedia]](https://en.wikipedia.org/wiki/ElasticSearch)
  * that means, you can use wget, curl , etc to interface it
  * but... there is also a webserver/web interface for it: http://hostname:9200

  ![ES REST API](img/9200.png)

# Description of the components: Logstash
  * Definition **log**: some data with a timestamp
  * Idea: take logs and send them somewhere and parse, filter, process them and send them to some destination (syslog, ElasticSearch, ...)

# logcollector
  * bot which takes AH data and sends it to logstash

# Project plan

![Project plan](img/metalab-world-domination.jpg)

# ok, seriously - some tasks

  * AbuseHelper Generic Event -- DONE
  * AbuseHelper Generic Sanitizer -- 95%
  * CyBox Mapping -- 30%
  * Mailing-list TrustedIntroducer -- 95%
  * Incident Handling Requirements Analysis document -- --> moved to abstract document w. CERT.org
  * ContactDB - CERTs Whois -- 50%
  * RTIR interface -- 10% (work by Hary Funet)
  * EventDB (ElasticSearch) Scalability -- 50%
  * EventDB SQL Support -- 0%
  * Enhanced Interface for Event Database -- 10% multiple proposals
  * regular workshops

# Results so far

  * logcollector bot (by Tomas)
  * ifas-deployment-tool ( by Tomas)
  * documentation (CERT-eu)
  * dashboards: 
    * by CERT.be
    * IFAS (by CERT-HK / Chris)
    * dashboard by Sindri / CERT.is


# IFAS (Information Feed Analysis System)
 
  * URL:  https://bitbucket.org/ifas/ifas/wiki/Home (private repo)
  ... slides by Chris ...

# ifas-deployment tool
  * [https://bitbucket.org/ahshare/ihap/src/](https://bitbucket.org/ahshare/ihap/src/)
  * URL: https://bitbucket.org/tomaslima/ifas-deployment-tool


# Next Developments

  * XXX 

# How to join us?!

  * mailing list: https://tiss.trusted-introducer.org/mailman/listinfo/abusehelper-share

# _BREAK_


# Demo Time
Time:  10:15 -- 12:30 
Speakers: Tomas, Aaron, Sindri, Chris 
Content:
  The speakers will provide a setup where participants can play around with it live and create their own dashboard.
  * Demo:
    * AbuseHelper
    * ElasticSearch
    * LogStash
    * Kibana
  * Demo of IFAS 
  * Demo by Sindri on Webinterface  (remote skype session with screen sharing) 


# _LUNCH BREAK_



# The detailed project plan: go through the list of the project plan
Time:  13:30  -- 14:00
Speakers: Cosmin, Tomas, Aaron
Content: a.k.a. "what is still missing, what do we want to achieve"?

  * Identified issues : description of the current tasks
  * Are there any tasks missing? Any features missing?
  * searching for participants - who could do which task?
  * Discussion about available resources ??


# _Short BREAK_


# Getting the demo to run on your own server
Time: 14:15 -- 16:15
Speakers: Tomas
Content: Demo of getting IFAS / Abusehelper running with the fabric automatic installer
  Participants will try out the IFAS deployment tool (fabric, cuisine) and install everything on their own servers. Requirements: one or two VMs in their own servers

