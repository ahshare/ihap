import os
from abusehelper.core.startup import Bot

xmpp_jid = 'ah@abusehelper.local'
xmpp_password = 'ah'
service_room = 'lobby'
enable_mailer = False

def basic(name, *args, **attrs):
    template = Bot.template(
        xmpp_jid=xmpp_jid,
        xmpp_password=xmpp_password,
        service_room=service_room,
        xmpp_ignore_cert = True,
        bot_state_file=os.path.join("state", name + ".state"),
        log_file=os.path.join("log", name + ".log"),
        xmpp_rate_limit=10
    )
    return template(name, *args, **attrs)

def configs():

    # Sanitizers
    for filename in os.listdir("./custom"):
        if filename.endswith(".sanitizer.py"):
            yield basic(filename[:-3], os.path.join("custom", filename))

   
    # System Bots
    yield basic("runtime", config="./runtime.py",  )


    # Source Bots
    yield basic("abusech", "abusehelper.contrib.abusech.abusechbot" )
    yield basic("dragonresearchgroup-ssh", "abusehelper.contrib.dragonresearchgroup.ssh" )
    yield basic("phishtank", "abusehelper.contrib.phishtank.phishtankbot", application_key="your key")


    # Expert Bots
    yield basic("combiner", "abusehelper.contrib.experts.combiner")
    yield basic("cymruexpert", "abusehelper.contrib.experts.cymruexpert")


    # Extra Bots
    yield basic("logcollector", "abusehelper.contrib.logcollector.logcollectorbot", logcollector_ip='192.168.59.200', logcollector_port=5000)    
