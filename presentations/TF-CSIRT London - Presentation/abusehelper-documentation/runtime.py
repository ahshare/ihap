import re
from abusehelper.core import rules
from abusehelper.core.runtime import Session
from startup import service_room as room_prefix

sources_room = room_prefix + ".sources"

def source(name, dst_room=None, **attrs):
    if dst_room is None:
        dst_room = room_prefix + ".source." + name

    # AbuseHelper "Chain"
    # ===================
    
    # Sources
    yield Session(name,
        dst_room=dst_room,
        **attrs)

    # Sources Sanitizer (AbuseCH, PhishTank)
    yield Session(name + ".sanitizer",
        src_room = dst_room,
        dst_room = "sanitized.sources.room") 

    # CymruExpert Enrichment
    yield Session("cymruexpert",				
        src_room = "sanitized.sources.room",
        dst_room = "cymruexpert.room") 

    # Merge All Inforamtion
    yield Session("combiner",				
        src_room = "sanitized.sources.room",
        augment_room = "cymruexpert.room",
        dst_room = "combiner.cymruexpert.room")  		

    # CymruExpert Sanitizer 
    yield Session("cymruexpert.sanitizer",			
        src_room = "combiner.cymruexpert.room",
        dst_room = sources_room) 

    
def consumer(name, src_room, **attrs):
    yield Session(name,
        src_room = src_room,
        **attrs)

def configs():
    yield source("abusech")
    yield source("dragonresearchgroup-ssh") 
    yield source("phishtank")

    yield consumer("logcollector", src_room=sources_room)
    
