curl -XPUT 127.0.0.1:9200/_template/template_abusehelper -d '
{
    "template" : "abusehelper*",
    "order" : 10,
    "settings" : {
        "refresh_interval" : 2
    },
    "mappings" : {
        "_default_" : {
            "_all" : {"enabled" : false},
           "properties" : {
              "@fields" : {
                    "type" : "object",
                    "path": "full",
                    "properties" : {
			            "source_name" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "source_desc" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "status" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "incident_class" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "incident_type" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "event_descr" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "event_notes" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "event_hash" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "ip" : { "type" : "ip", "index": "not_analyzed", "null_value" : "0.0.0.0" },
			            "netblock" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "bgp" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "asn" : { "type" : "integer", "null_value" : 0, "store" : "yes"},
			            "as_name" : { "type": "string", "index": "not_analyzed", "null_value" : "NA", "store" : "yes" },
			            "country_code" : { "type": "string", "index": "not_analyzed", "null_value" : "NA", "store" : "yes" },
			            "host" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" },
			            "url" : { "type": "string", "index": "not_analyzed", "null_value" : "NA" }
                   }
              }
           }
        }
   }
}
'
