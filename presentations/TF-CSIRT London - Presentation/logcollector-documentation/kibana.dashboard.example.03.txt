{
  "title": "Your Basic Dashboard",
  "services": {
    "query": {
      "idQueue": [
        1,
        2,
        3,
        4
      ],
      "list": {
        "0": {
          "query": "*",
          "alias": "",
          "color": "#7EB26D",
          "id": 0,
          "pin": false,
          "type": "lucene"
        }
      },
      "ids": [
        0
      ]
    },
    "filter": {
      "idQueue": [
        1,
        2
      ],
      "list": {
        "0": {
          "from": "2013-09-07T06:17:35.320Z",
          "to": "2013-09-09T06:17:35.320Z",
          "field": "@timestamp",
          "type": "time",
          "mandate": "must",
          "active": true,
          "alias": "",
          "id": 0
        }
      },
      "ids": [
        0
      ]
    }
  },
  "rows": [
    {
      "title": "Options",
      "height": "50px",
      "editable": true,
      "collapse": false,
      "collapsable": true,
      "panels": [
        {
          "error": false,
          "span": 3,
          "editable": true,
          "group": [
            "default"
          ],
          "type": "dashcontrol",
          "save": {
            "gist": false,
            "elasticsearch": true,
            "local": true,
            "default": true
          },
          "load": {
            "gist": true,
            "elasticsearch": true,
            "local": true
          },
          "hide_control": false,
          "elasticsearch_size": 20,
          "temp": true,
          "temp_ttl": "30d",
          "ttl_enable": true
        },
        {
          "error": false,
          "span": 4,
          "editable": true,
          "group": [
            "default"
          ],
          "type": "text",
          "status": "Stable",
          "mode": "markdown",
          "content": "The dashcontrol panel to the left lets you save this dashboard to Elasticsearch once you have it how you like it. See the note on the welcome page about setting a global default.",
          "style": {},
          "title": "The dashcontrol panel"
        },
        {
          "error": false,
          "span": 5,
          "editable": true,
          "group": [
            "default"
          ],
          "type": "text",
          "status": "Stable",
          "mode": "markdown",
          "content": "If you have a field with a timestamp in it, you might want to add a 'timepicker' panel here. Click the cog icon over to the left to do so. You can also remove these information text panels there",
          "style": {},
          "title": "Have a timestamp somewhere?"
        }
      ]
    },
    {
      "title": "Query",
      "height": "50px",
      "editable": true,
      "collapse": false,
      "collapsable": true,
      "panels": [
        {
          "error": false,
          "span": 5,
          "editable": true,
          "group": [
            "default"
          ],
          "type": "query",
          "label": "Search",
          "history": [
            "*"
          ],
          "remember": 10,
          "pinned": true,
          "query": "*"
        },
        {
          "error": false,
          "span": 7,
          "editable": true,
          "group": [
            "default"
          ],
          "type": "text",
          "status": "Stable",
          "mode": "markdown",
          "content": "#### Filtering\nSee the small *Filters* text to the left below? Click it to expand the filters row. Right now there are none. click on one of the icons in the document types list to filter down to only that document type",
          "style": {}
        }
      ]
    },
    {
      "title": "Filters",
      "height": "50px",
      "editable": true,
      "collapse": true,
      "collapsable": true,
      "panels": [
        {
          "error": false,
          "span": 12,
          "editable": true,
          "group": [
            "default"
          ],
          "type": "filtering"
        }
      ]
    },
    {
      "title": "CERT.PT",
      "height": "250px",
      "editable": true,
      "collapse": false,
      "collapsable": true,
      "panels": [
        {
          "error": false,
          "span": 6,
          "editable": true,
          "type": "map",
          "queries": {
            "mode": "all",
            "ids": [
              0
            ]
          },
          "map": "world",
          "colors": [
            "#A0E2E2",
            "#265656"
          ],
          "size": 100,
          "exclude": [],
          "spyable": true,
          "index_limit": 0,
          "title": "CERT.PT Map",
          "field": "@fields.country_code"
        },
        {
          "error": "",
          "span": 3,
          "editable": true,
          "type": "timepicker",
          "status": "Stable",
          "mode": "relative",
          "time_options": [
            "5m",
            "15m",
            "1h",
            "6h",
            "12h",
            "24h",
            "2d",
            "7d",
            "30d"
          ],
          "timespan": "2d",
          "timefield": "@timestamp",
          "timeformat": "",
          "refresh": {
            "enable": false,
            "interval": 30,
            "min": 3
          },
          "title": "TimePicker",
          "filter_id": 0
        }
      ]
    },
    {
      "title": "Events",
      "height": "650px",
      "editable": true,
      "collapse": false,
      "collapsable": true,
      "panels": [
        {
          "error": false,
          "span": 12,
          "editable": true,
          "group": [
            "default"
          ],
          "type": "table",
          "size": 100,
          "pages": 5,
          "offset": 0,
          "sort": [
            "@fields.incident_type",
            "asc"
          ],
          "style": {
            "font-size": "9pt"
          },
          "overflow": "min-height",
          "fields": [
            "@fields.asn",
            "@fields.incident_class",
            "@fields.incident_type",
            "@fields.country_code",
            "@fields.source_name"
          ],
          "highlight": [],
          "sortable": true,
          "header": true,
          "paging": true,
          "spyable": true,
          "queries": {
            "mode": "all",
            "ids": [
              0
            ]
          },
          "field_list": true,
          "status": "Stable",
          "trimFactor": 300,
          "normTimes": true
        }
      ]
    }
  ],
  "editable": true,
  "index": {
    "interval": "none",
    "pattern": "[logstash-]YYYY.MM.DD",
    "default": "_all"
  },
  "style": "dark",
  "failover": false
}