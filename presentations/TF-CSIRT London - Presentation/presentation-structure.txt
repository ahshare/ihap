Title: Incident handling automation - an update from CERT.at and CERT.pt

== AbuseHelper ==
- AbuseHelper overview
- What CERT Community needs from AbuseHelper


== Solution: LogStash + ElasticSearch + Kibana ==
- LogStash Overview
- ElasticSearch Overview
- Kibana Overview


== CERT.PT, CERT.AT and CERT-EU Developments: ==
- Documents:
    * Incident Handling Requirements Analysis Document
    * Abusehelper Installation Guide on Ubuntu Server Document
- Code Development:
    * LogcollectorBot in AbuseHelper repository
    * GenericSanitizer (Data Harmonization) in AbuseHelper repository
- Cooperation Results:
    * Screenshots from our tests with AbuseHelper, GenericSanitizer, LogcollectorBot, LogStash, ElasticSearch and Kibana
    * Video (in case if demo fail and of course, can be use to share in private thorugh youtube)
    * Demo


== AbuseHelper Share Repository ==
- (paste the description from repo)


== What we want? ==
- next goals: cooperation, abusehelper nodes share data, more participation from CERTs, shares, etc


== Next Goals ==
Import and Export in STIX format and may be in X-ARF format
Integration with ContacDB




== Notes ==
Check documents: AH Requirements, CERT-EU Presentation

In the end of apresentation I can share 2 VirtualMachines with 2 Specific Documentation folders (all 2GB) for who is interested.
I need to write the FAST DOC for who just want to run and see results.
